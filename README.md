Exercitii-Rezolvate in c#
P1. Write a program which takes an integer as input and prints the sum of its digits. 
private void button1_Click(object sender, EventArgs e)
        {
            int num=int.Parse(Interaction.InputBox("Introdu un numar"));

            int sum = 0;

            while(num > 0)
            {
                sum += num % 10;
                num /= 10;
            }

            MessageBox.Show("Suma numerelor este " + sum);
        }
		
P2. Write a program which takes 2 clock times and establish an order relationship between these. Print the relationship as in the following example
private void button2_Click(object sender, EventArgs e)
        {
            TimeSpan t1 = new TimeSpan(int.Parse(Interaction.InputBox("Introdu ora la primul timp")), int.Parse(Interaction.InputBox("Introdu minutul la primul timp")), 0);
            TimeSpan t2 = new TimeSpan(int.Parse(Interaction.InputBox("Introdu ora la al doilea timp")), int.Parse(Interaction.InputBox("Introdu minutul la al doilea timp")), 0);

            if (t1 < t2)
                MessageBox.Show(t1.Hours + ":" + t1.Minutes  + "<" + t2.Hours + ":" + t2.Minutes);
            else if(t1>t2)
                MessageBox.Show(t1.Hours + ":" + t1.Minutes + ">" + t2.Hours + ":" + t2.Minutes);
            else
                MessageBox.Show(t1.Hours + ":" + t1.Minutes + "=" + t2.Hours + ":" + t2.Minutes);

            
        }
P3. Given 2 arrays of distinct names, a and b, print the names that are found only in a and not in b.	
 private void button3_Click(object sender, EventArgs e)
        {
            string[] g1 = { "Ion", "Vasile", "Alex", "Andra", "Georgiana" };
            string[] g2 = { "Andreea", "Irina", "Vasile", "Georgiana" };

            string[] unCommon = new string[0];

            for(int i = 0;i <g1.Length;i++)
            {
                bool gasit = false;
                for(int j = 0;j < g2.Length;j++)
                {
                    if(g1[i] == g2[j])
                    {
                        gasit = true;           
                    }
                }
                if(!gasit)
                {
                    Array.Resize(ref unCommon, unCommon.Length + 1);
                    unCommon[unCommon.Length - 1] = g1[i];
                }
            }
            for (int i = 0; i < unCommon.Length; i++)
                MessageBox.Show(unCommon[i]);


        }

P5. HTML --> Table
<html>
<link rel ="stylesheet" type="text/css" href="style.css">

<table>
	<tr>
		<th>First Name</th>
		<th>Last Name</th>
		<th>Email</th>
		<th>Score</th>
	</tr>
	<tr>
		<th>John</th>
		<th>Smith</th>
		<th>john.smith@yahoo.ro</th>
		<th>86</th>
	</tr>
	<tr>
		<th>Jane</th>
		<th>Doe</th>
		<th>jane.doe@gmail.com</th>
		<th>100</th>
	</tr>
</table>


</html>
		